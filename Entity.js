// Created by: Martin E. Labanic,
// Email: martin.labanic@gmail.com
// Website: www.theboxthatwasnt.com 

// Entity Object
var Entity = function(x, y, prevXPos, width, height, xForce, yForce, alive, direction, frame, mvSpeed, jmpSpeed, onGround) {
    this.x = x;                 // int
    this.y = y;                 // int
    this.prevXPos = prevXPos;   // int
    this.width = width;         // int
    this.height = height;       // int
    this.xForce = xForce;       // int
    this.yForce = yForce;       // int

    this.alive = alive;
    this.direction = direction; // int   left = -1, right = 1
    this.frame = frame;
    this.mvSpeed = mvSpeed;     // int
    this.jmpSpeed = jmpSpeed;   // int
    this.onGround = onGround;   // boolean
};