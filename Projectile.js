// Created by: Martin E. Labanic,
// Email: martin.labanic@gmail.com
// Website: www.theboxthatwasnt.com 

// Projectile Object
var Projectile = function(x, y, xForce, width, height, active, startX) {
    this.x = x;
    this.y = y;
    this.xForce = xForce;
    this.width = width;
    this.height = height;
    this.active = active;
    this.startX = startX;
};