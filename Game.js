// Created by: Martin E. Labanic,
// Email: martin.labanic@gmail.com
// Website: www.theboxthatwasnt.com 

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// VARIABLES /////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //==================================================================//
    //////// Misc ////////////////////////////////////////////////////////
    //==================================================================//
    var context;
   // var canvas1;
    var timerTimeOut;
    var gameTimeOut;
    var countDownSeconds;   // For our count down timer
    var gamePaused = false;
    var projectiles = new Array();
    var score;
    var playing = false;
    var fuel;
    var win;

    //==================================================================//
    //////// Images //////////////////////////////////////////////////////
    //==================================================================//
    var gTile = new Image();	// Grass image
    var dTile = new Image();	// Dungeon Wall image
    var cTile = new Image();    // Planet Core image
    var bTile = new Image();    // Blue tile image
    var yTile = new Image();    // Yellow tile image
    var pTile = new Image();    // Pink tile image
    var oTile = new Image();    // Orange tile
    var fTile = new Image();    // Fuel image

    var laserImg = new Image(); // Laser image
    var shipImg = new Image();  // Ship image
    var beamImg = new Image();
    var background = new Image();

    var startScreenImg = new Image();
    var winImg = new Image();
    var loseImg = new Image();

    var fuel100 = new Image();
    var fuel75 = new Image();
    var fuel50 = new Image();
    var fuel25 = new Image();
    var fuel10 = new Image();
    var fuel0 = new Image();

    var playerAnimations = [
        [null, null],
        [null, null],
        [null, null],
        [null, null]
    ];

    var enemyAnimations = [null, null];

    //==================================================================//
    //////// Sound Effects ///////////////////////////////////////////////
    //==================================================================//
    var soundBackgroundMusic;
    var soundLaser;
    var soundJetpack;

    //==================================================================//
    //////// Player //////////////////////////////////////////////////////
    //==================================================================//
    var player = new Entity(0, 0, null, 18, 18, 0, 0, true, 1, 0, 2, -4, false);

    // Player states
    var state_upKeyDown = false;
    var state_rightKeyDown = false;
    var state_leftKeyDown = false;
    var state_downKeyDown = false;
    var state_dKeyDown = false;

    //==================================================================//
    //////// Enemies /////////////////////////////////////////////////////
    //==================================================================//
    var enemies = new Array();

    //==================================================================//
    //////// Level ///////////////////////////////////////////////////////
    //==================================================================//

    var ship;

    // Init level to be 30 x 30
    var level = [
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'],
        ['E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E']
    ];

    var caveOne = [
        ['D', 'D', 'D', 'D', 'D'],
        ['D', 'E', 'E', 'E', 'D'],
        ['D', 'E', 'C', 'D', 'D'],
        ['D', 'D', 'D', 'D', 'G']
    ];

    var caveTwo = [
        ['D', 'G', 'D', 'D', 'D'],
        ['G', 'D', 'C', 'E', 'D'],
        ['G', 'D', 'D', 'D', 'D'],
        ['D', 'G', 'G', 'G', 'D']
    ];

    var caveThree = [
        ['G', 'D', 'D', 'D', 'D'],
        ['G', 'D', 'D', 'C', 'G'],
        ['D', 'D', 'D', 'D', 'D'],
        ['G', 'D', 'D', 'D', 'D']
    ];

    var caveFour = [
        ['G', 'D', 'D', 'G', 'G'],
        ['G', 'D', 'C', 'D', 'G'],
        ['G', 'E', 'D', 'G', 'G'],
        ['G', 'D', 'G', 'G', 'G']
    ];

    var caveFive = [
        ['D', 'D', 'D', 'D', 'D'],
        ['D', 'E', 'G', 'E', 'D'],
        ['D', 'C', 'G', 'D', 'G'],
        ['D', 'D', 'D', 'G', 'G']
    ];

    var levelAttr = new Array();

    //==================================================================//
    //////// Constants ///////////////////////////////////////////////////
    //==================================================================//
    var constResX = 330;	// Canvas resolution x axis
    var constResY = 330;	// Canvas resolution y axis
    var constMapWidth = level[0].length;	// Default map width / number of COLUMNS
    var constMapHeight = level.length;	    // Default map height / number of ROWS
    var constTileWidth = 22;	// Default tile width
    var constTileHeight = 22;	// Default tile height
    var constEMPTY = 0;
    var constPASSABLE = 1;
    var constIMPASSABLE = 2;


    // Refresh rate
    var constRefreshRate = (1000 / 45);
    var constTimerStart = 16;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// FUNCTIONS - Initialisations & The Endgame /////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    window.onload = function () {
        init();
    };


    // Basic game initialisation
    // Preconditions: None
    function init() {
        window.addEventListener('keydown', doActionKeyDown, false);
        window.addEventListener('keyup', doActionKeyUp, false);
      //  canvas1 = document.getElementById('canvas');
        context = document.getElementById('canvas').getContext('2d');
        
        
        initImages();
        initGame();
        displayStartScreen();
    }


    // Initialise the images that will be used
    // Preconditions: None
    function initImages() {
    	context.fillStyle = "White";
        context.font = "12pt Courier";
        context.fillText("Loading Images." , context.offsetX+140, context.offsetY+(constResY/2)+5);
        gTile.src = 'art/grassTile.png';
        dTile.src = 'art/dWallTile.png';
        bTile.src = 'art/blueTile.png';
        yTile.src = 'art/yellowTile.png';
        pTile.src = 'art/pinkTile.png';
        cTile.src = 'art/planetCoreTile.png';
        fTile.src = 'art/fuelTile.png';
        oTile.src = 'art/orangeTile.png';

        fuel100.src = 'art/fuel100.png';
        fuel75.src = 'art/fuel75.png';
        fuel50.src = 'art/fuel50.png';
        fuel25.src = 'art/fuel25.png';
        fuel10.src = 'art/fuel10.png';
        fuel0.src = 'art/fuel0.png';

        laserImg.src = 'art/laser.png';
        shipImg.src = 'art/ship.png';
        beamImg.src = 'art/beam.png';
        background.src = 'art/background.gif';

        startScreenImg.src = 'art/intro.png';
        winImg.src = 'art/win.png';
        loseImg.src = 'art/lose.png';


        playerAnimations[0][0] = new Image();
        playerAnimations[0][0].src = 'art/runningLeftOne.png';

        playerAnimations[0][1] = new Image();
        playerAnimations[0][1].src = 'art/runningLeftTwo.png';

        playerAnimations[1][0] = new Image();
        playerAnimations[1][0].src = 'art/runningRightOne.png';

        playerAnimations[1][1] = new Image();
        playerAnimations[1][1].src = 'art/runningRightTwo.png';

        playerAnimations[2][0] = new Image();
        playerAnimations[2][0].src = 'art/jumpingRightOne.png';

        playerAnimations[2][1] = new Image();
        playerAnimations[2][1].src = 'art/jumpingRightTwo.png';

        playerAnimations[3][0] = new Image();
        playerAnimations[3][0].src = 'art/jumpingLeftOne.png';

        playerAnimations[3][1] = new Image();
        playerAnimations[3][1].src = 'art/jumpingLeftTwo.png';

        enemyAnimations[0] = new Image();
        enemyAnimations[0].src = 'art/enemyOne.png';

        enemyAnimations[1] = new Image();
        enemyAnimations[1].src = 'art/enemyTwo.png';
        clear();
        context.fillText("Done Loading Images." , context.offsetX+140, context.offsetY+(constResY/2)+5);
    }


    // Initialise & resets the game
    // Preconditions: None
    function initGame() {
        // clear out the old game
        clearInterval(gameTimeOut);
        clearTimeout(timerTimeOut);
        win = null;

        score = 1;
        fuel = 250;
        countDownSeconds = constTimerStart;

        player.alive = true;
        player.x = (Math.random() * 400) + 100;
        player.y = -350;
        player.prevXPos = player.x;
        player.frame = 0;

        if (playing) { // We want to reset the game
            resetLevelWith('E');
            removeAllEnemies();
            removeAllProjectiles();
            context.restore();
            context.save();
        } else {
            context.save();
            soundBackgroundMusic = document.getElementById('soundMainMusic');
            soundBackgroundMusic.currentTime = 0;
            soundBackgroundMusic.play();
            soundLaser = document.getElementById('soundLaser');
            soundJetpack = document.getElementById('soundJetpack');
        }

        context.offsetX = player.x - 156;
        context.offsetY = player.y - 156;
        ship = new Projectile(player.x - 150, -400, 0, 300, 100, 0, 0);
        context.translate(-(player.x - 156), -(player.y - 156));
        generateLevel('A');
    }


    // Start the game
    // Preconditions: None
    function startGame() {
        playing = true;
        gameTimeOut = setInterval('animate()', constRefreshRate);
    }


    // End the current level
    // Preconditions: None
    function endGame() {
        clearInterval(gameTimeOut);
        clearTimeout(timerTimeOut);
       // playing = false;
        if (!win) {
            displayLoseScreen();
        } else {
            displayWinScreen();
        }
    }


    // Pauses / unpauses a game
    // Preconditions: None
    function pause() {
        if (gamePaused && countDownSeconds > 0) {
            startGame();
            if (countDownSeconds < constTimerStart) {
                updateTimer();
            }
            gamePaused = false;
        } else {
            gamePaused = true;
            clearInterval(gameTimeOut);
            clearTimeout(timerTimeOut);
        }
    }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// FUNCTIONS - Actions ///////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // User input; fire when the user presses a key on the keyboard
    // Preconditions: None
    // List of keyCodes: http://www.aspdotnetfaq.com/Faq/What-is-the-list-of-KeyCodes-for-JavaScript-KeyDown-KeyPress-and-KeyUp-events.aspx
    function doActionKeyDown(event) {
        switch (event.keyCode) {
            case 37:    // User moving left
                state_rightKeyDown = false;
                state_downKeyDown = false;
                state_leftKeyDown = true;
                break;

            case 39:    // User moving right
                state_leftKeyDown = false;
                state_downKeyDown = false;
                state_rightKeyDown = true;
                break;

            case 38:    // User moving is jumping
                state_downKeyDown = false;
                state_upKeyDown = true;
                break;

            case 40:    // User moving down
                state_upKeyDown = false;
                state_leftKeyDown = false;
                state_rightKeyDown = false;
                state_downKeyDown = true;
                break;

            case 68:    // User pressed 'd', we be drilling
                state_dKeyDown = true;
                break;

            case 83:    // User pressed 's', we be shooting
                shoot(player);
                break;

            case 27:    // Pause / unpause the game
                pause();
                break;

            case 13:    // User wants to start / reset the game
                if (!playing) {
                    startGame();
                } else {
                    initGame();
                    startGame();
                }
                break;

            default:
                break;
        }
    }


    // User input; fire when the user releases a key on the keyboard
    // Preconditions: None
    function doActionKeyUp(event) {
        switch (event.keyCode) {
            case 37:    // User stops moving left
                state_leftKeyDown = false;
                break;

            case 39:    // User stops moving right
                state_rightKeyDown = false;
                break;

            case 38:    // User stops moving up
                state_upKeyDown = false;
                break;

            case 40:    // User stops moving down
                state_downKeyDown = false;
                break;

            case 68:    // User stops drilling
                state_dKeyDown = false;
                break;

            default:
                break;
        }
    }


    // Make an entity jump
    // Preconditions: None
    function jump(entity) {
        entity.yForce = entity.jmpSpeed;
        //  soundJetpack.currentTime = 0.18;
        //   soundJetpack.play();
    }


    // Drill into blocks
    // Preconditions: None
    function drill(entity) {
        // Do not drill if we are jumping
        if (entity.onGround && withinXLevelBounds(entity) && withinYLevelBounds(entity)) {
            // Locate our tiles
            var leftTile = getLeftTile(entity.x);
            var rightTile = getRightTile(entity.x);
            var botTile = getBottomTile(entity.y);

            if (state_leftKeyDown) {
                if (getTileType(getTile(botTile - 1, leftTile)) != constEMPTY) {
                    replaceTileWith(botTile - 1, leftTile, 'E');
                }
            }
            // Check if were drilling to the right
            else if (state_rightKeyDown) {
                if (getTileType(getTile(botTile - 1, rightTile)) != constEMPTY) {
                    replaceTileWith(botTile - 1, rightTile, 'E');
                }
            }
            // Else drill downwards
            else if (state_downKeyDown) {
                // Check it we won't fall through the world
                if (getTileType(getTile(botTile, rightTile - 1)) != constEMPTY) {
                    replaceTileWith(botTile, leftTile + 1, 'E');
                }
            }
        }
    }


    // Allow the entity passed in to shoot a projectile
    // Preconditions: None
    function shoot(entity) {
        if (projectiles.length < 5) {
            var projectile;
            if (entity.direction == 1) {    // Shoot to the right
                projectile = new Projectile(entity.x + entity.width, entity.y + (entity.height / 2) - 1, 3, 4, 3, true, (entity.x + entity.width));
            } else {    // Shoot the the left
                projectile = new Projectile(entity.x - 1, entity.y + (entity.height / 2) - 1, -3, 4, 3, true, entity.x - 1);
            }
            projectiles.push(projectile);

            // Play the laser sound
            soundLaser.currentTime = 0;
            soundLaser.play();
        }
    }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// FUNCTIONS - Level /////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //==================================================================//
    //////// Level Generation ////////////////////////////////////////////
    //==================================================================//

    // Generate the level
    // Preconditions: None
    function generateLevel(levelSeed) {
        // Generate the level
        // TODO: Incorporate level seed.
        var randomNum;
        var randomTunnel = Math.floor(Math.random()*15)+5;
        for (var i = 2; i < constMapHeight; i++) {
            if (i != randomTunnel & i != 29) {
                for (var j = 2; j < constMapWidth - 2; j++) {
                    randomNum = Math.random();
                    if (randomNum > .5) {
                        level[i][j] = getRandomTile();
                    } else if (level[i - 1][j] != 'E') {
                        level[i][j] = getRandomTile();
                    }
                }
            }
        }

        // TODO: Set the level attributes
        generateLevelAttr(levelSeed);
        generateCave();
        generateFuelTiles(10);
        generateEnemies(30);
    }


    // Generate a cave
    // Preconditions: None
    function generateCave() {
        var startX = Math.floor(Math.random()*10)+10;
        var startY = Math.floor(Math.random()*10)+10;
        var cave = Math.floor(Math.random()*5);

        // Pick the cave we want
        if(cave == 0) {
            cave = caveOne;
        } else if (cave == 1) {
            cave = caveTwo;
        } else if (cave == 2) {
            cave = caveThree;
        } else if (cave == 3) {
            cave = caveFour;
        } else {
            cave = caveFive;
        }

        // Place it into the level
        for (var i = startX; i < startX + caveOne.length; i++) {
            for (var j = startY; j < startY + caveOne[0].length; j++) {
                level[i][j] = cave[i % startX][j % startY];
            }
        }
    }


    // Generate a cave
    // Preconditions:
    function generateLevelAttr(input) {

    }


    // Generate some enemies on the level
    // Preconditions: None
    function generateEnemies(numToGen) {
        var randomX;
        var randomY;
        var randomFrame;

        for (var i = 0; i < numToGen; i++) {
            randomX = Math.floor(Math.random() * 26) + 2;
            randomY = Math.floor(Math.random() * 26) + 2;
            randomFrame = Math.round(Math.random());

            if (!isCoreTile(randomY, randomX)) {
                enemies.push(new Entity(randomX * constTileWidth, randomY * constTileHeight, null, 18, 18, 0, 0, true, 1, randomFrame, 2, -4, false));
                replaceTileWith(randomY, randomX, 'E');
            }
        }

        // Remove any null enemies
        for (var j = 0; j < enemies.length; j++) {
            if (enemies[j] == null) {
                enemies.splice(j, 1);
            }
        }
    }


    // Generates a number of fuel tiles
    // Preconditions: None
    function generateFuelTiles(numToGen) {
        for (var i = 0; i < numToGen; i++) {
            var randomX = Math.floor(Math.random() * 26) + 2;
            var randomY = Math.floor(Math.random() * 8) + 18;

            if (!isCoreTile(randomY, randomX)) {
                replaceTileWith(randomY, randomX, 'F');
            }
        }
    }

    function generateTrapTile() {

    }

    //==================================================================//
    //////// Level Accessors /////////////////////////////////////////////
    //==================================================================//

    // Return a specific tile at coordinate (x,y) in the level array
    // Preconditions: None
    function getTile(x, y) {
        return level[x][y];
    }


    // Returns if the tile is empty, passable, or impassable
    // Preconditions: None
    function getTileType(tile) {
//        Disabled for efficiency
//        var tileType;
        if (tile == 'E') {   // Empty tile
            return constEMPTY;
        } else if (tile == 'G') {  // Grass tile
            return constIMPASSABLE;
        } else if (tile == 'D') {   // Dungeon tile
            return constIMPASSABLE;
        } else if (tile == 'C') {   // Core Tile
            return constIMPASSABLE;
        } else if (tile == 'F') {   // Fuel Tile
            return constIMPASSABLE;
        } else if (tile == 'B') {   // Blue tile
            return constIMPASSABLE;
        } else if (tile == 'Y') {   // Yellow tile
            return constIMPASSABLE;
        } else if (tile == 'P') {   // Pink tile
            return constIMPASSABLE;
        }
    }


    // Return an image of the specified type except for empty types
    // Preconditions: None
    function getTileImg(type) {
        // Tile image data we will return
//    	var tileToShow = new Image();
        if (type == 'G') {
            return gTile;
        } else if (type == 'D') {
            return dTile;
        } else if (type == 'C') {
            return cTile;
        } else if (type == 'F') {
            return fTile;
        } else if (type == 'B') {
            return bTile;
        } else if (type == 'Y') {
            return yTile;
        } else if (type == 'P') {
            return pTile;
        }
//		return tileToShow;
    }

    // Returns a random tile type that is not fuel or the core
    // Preconditions: None
    function getRandomTile() {
        var random = Math.random();
        var tile;
        if (random > .7) {
            tile = 'G';
        } else if (random > .6) {
            tile = 'D';
        } else if (random > .4) {
            tile = 'B';
        } else if (random > .2){
            tile = 'Y';
        } else {
            tile = 'P';
        }
        return tile;
    }


    // Return whether or not the core was found
    // Preconditions: None
    function isCoreTile(x, y) {
        return getTile(x, y) == 'C';
    }

    function isFuelTile(x, y) {
        return getTile(x, y) == 'F';
    }

    //==================================================================//
    //////// Level Mutators //////////////////////////////////////////////
    //==================================================================//

    // Replaces a tile within the level array
    // Preconditions: None
    function replaceTileWith(x, y, tileType) {
        if (x > -1 && x < constMapHeight && y > -1 && y < constMapWidth) {
            // If the player is breaking the core then start the endgame
            if (isCoreTile(x, y)) {
                updateTimer();
            } else if (isFuelTile(x, y)) {
                // Add fuel for the user if they break a fuel tile
                if (fuel + 40 < 250) {
                    fuel += 40;
                } else {
                    fuel = 250;
                }
            }
            level[x][y] = tileType;
        }
    }

    // Replaces every tile in the level with a provided type
    // Preconditions: None
    function resetLevelWith(tileType) {
        for (var i = 0; i < level.length; i++) {
            for (var j = 0; j < level[i].length; j++) {
                level[i][j] = tileType;
            }
        }
    }


    // Removes all the enemies from the enemies array
    // Preconditions: None
    function removeAllEnemies() {
        enemies.splice(0, enemies.length);
    }

    // Removes all the projectiles in the projectiles array
    // Preconditions: None
    function removeAllProjectiles() {
        projectiles.splice(0, projectiles.length);
    }


    function generateRandomTiles(type, numToGen) {
        for (var i = 0; i < numToGen; i++) {
            var randomX = Math.floor(Math.random() * 26) + 2;
            var randomY = Math.floor(Math.random() * 8) + 18;

            if (!isCoreTile(randomY, randomX)) {
                replaceTileWith(randomY, randomX, type);
            }
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// FUNCTIONS - Animation /////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Animate the game
    // Preconditions: None
    function animate() {
        // the timer has ended, end the game.
        if (win == null) {
            if (countDownSeconds < 1) {
                win = false;
            } else {
                // Update our users
                updatePlayer();
                updateEnemies();
                updateProjectiles();
                updateView();
            }

            // Check if the player is still alive before we redraw anything
            if (!player.alive) {
                win = false;
            } 

            // Clear the screen
            clear();

            // Draw everything
            drawBackground();
            drawProjectiles();
            drawLevel();
            drawPlayer();
            drawShip();
            drawEnemies();
            drawHUD();
        }
        if (win != null) {
            endGame();
        }
    }


    // Retrieve the appropriate player frame
    // Preconditions: None
    function getPlayerFrame() {
        var frameToShow;
        // Check if were jumping
        if (state_upKeyDown && fuel > 0) {
            switch (player.direction) {
                case 1:     // Jumping right
                    frameToShow = playerAnimations[2][player.frame];
                    if (player.frame == 0) {
                        player.frame = 1;
                    } else {
                        player.frame = 0;
                    }
                    break;

                case -1:    // Jumping left
                    frameToShow = playerAnimations[3][player.frame];
                    if (player.frame == 0) {
                        player.frame = 1;
                    } else {
                        player.frame = 0;
                    }
                    break;

                default:
                    break;
            }
        }

        // Check if we have not moved
        else if (player.prevXPos == player.x) {
            if (player.direction == 1) {    // Right
                frameToShow = playerAnimations[1][player.frame];
            } else {
                frameToShow = playerAnimations[0][player.frame];
            }
        }

        // Else were moving horizontaly
        else {
            switch (player.direction) {
                case 1:     // Moving right
                    frameToShow = playerAnimations[1][player.frame];
                    if (player.onGround) {
                        if (player.frame == 0) {
                            player.frame = 1;
                        } else {
                            player.frame = 0;
                        }
                    }

                    break;

                case -1:    // Moving left
                    frameToShow = playerAnimations[0][player.frame];
                    if (player.onGround) {
                        if (player.frame == 0) {
                            player.frame = 1;
                        } else {
                            player.frame = 0;
                        }
                    }
                    break;

                default:
                    break;
            }
        }
        return frameToShow;
    }


    // Retrieve the appropriate frame for the enemy
    // Preconditions: None
    function getEnemyFrame(entityIndex) {
        var frameToShow = enemyAnimations[enemies[entityIndex].frame];
        if (enemies[entityIndex].frame == 1) {
            enemies[entityIndex].frame = 0;
        } else {
            enemies[entityIndex].frame = 1;
        }
        return frameToShow;
    }


    // Retrieve the approriate frame for the fuel display
    // Preconditions: None
    function getFuelFrame() {
        var t = fuel / 2.5;
        if (t > 75) {
            return fuel100;
        } else if (t > 50) {
            return fuel75;
        } else if (t > 25) {
            return fuel50;
        } else if (t > 10) {
            return fuel25;
        } else if (t > 0) {
            return fuel10;
        } else {
            return fuel0;
        }
    }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// FUNCTIONS - Drawing ///////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Draw the background for the game
    // Preconditions: None
    function drawBackground() {
   //     context.save();
        context.drawImage(background, context.offsetX, context.offsetY);
  //      context.fillStyle = "grey";
 //       context.fillRect(context.offsetX, context.offsetY, constResX, constResY);
  //      context.restore();
    }


    // Draw the HUD for the game
    // Preconditions: None
    function drawHUD() {
        context.strokeRect(context.offsetX, context.offsetY, constResX, constResY);
        context.drawImage(getFuelFrame(), context.offsetX + 10, context.offsetY + 10);
        if (countDownSeconds < constTimerStart) {
            context.save();
            context.fillStyle = "red";
            context.font = "40pt Courier";
            context.fillText(countDownSeconds.toString(), context.offsetX + 140, context.offsetY + 45);
            context.restore();
        }

    }


    // Draw the level according to where the user is on the level
    // Preconditions: None
    // Note: can be optimised more but no time
    function drawLevel() {
        // j = column, i = row
        for (var i = 0; i < constMapHeight; i++) {
            for (var j = 0; j < constMapWidth; j++) {
                // If not an empty tile & tile is on screen
                if (getTile(i, j) != 'E' && onScreen(j * constTileWidth, i * constTileHeight, 20)) {
                    context.drawImage(getTileImg(getTile(i, j)), j * constTileWidth, i * constTileHeight, constTileWidth, constTileHeight);
                }
            }
        }
    }


    // Draw the player on the level
    // Preconditions: None
    function drawPlayer() {
        // Draw the player
        context.drawImage(getPlayerFrame(), player.x, player.y, player.width, player.height);
    }


    // Draws the ship
    // Preconditions: None
    function drawShip() {
        if (countDownSeconds < constTimerStart) {
            context.drawImage(beamImg, (ship.x + (ship.width/2) - 15), ship.y + ship.height-2);
        }
        context.drawImage(shipImg, ship.x, ship.y);
    }


    // Draw the enemies on the level
    // Preconditions: None
    function drawEnemies() {
        for (var i = 0; i < enemies.length; i++) {
            if (onScreen(enemies[i].x, enemies[i].y, 18)) {
                context.drawImage(getEnemyFrame(i), enemies[i].x, enemies[i].y, enemies[i].width, enemies[i].height);
            }
        }
    }


    // Draw the projectiles
    // Preconditions: None
    function drawProjectiles() {
        for (var i = 0; i < projectiles.length; i++) {
            context.drawImage(laserImg, projectiles[i].x, projectiles[i].y, projectiles[i].width, projectiles[i].height);
        }
    }


    // Clear the visible sections of the canvas
    // Preconditions: None
    function clear() {
        context.clearRect(context.offsetX, context.offsetY, constResX, constResY);
    }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// FUNCTIONS - Updates ///////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Update the view to follow the player
    // Preconditions: None
    function updateView() {
        context.offsetX += player.xForce;
        context.offsetY += player.yForce;
        context.translate(-player.xForce, -player.yForce);
    }


    // Update the player entity
    // Preconditions: None
    function updatePlayer() {
        // Update x axis movement
        // Stop x movement if none of the arrow keys are pressed down
        player.prevXPos = player.x;
        if (!(state_rightKeyDown && state_leftKeyDown)) {
            player.xForce = 0;
        }

        if (state_dKeyDown) {   // We be drilling
            drill(player);
        }

        // Player moving left
        if (state_leftKeyDown) {
            player.xForce = -player.mvSpeed;
            player.direction = -1;
        }
        // Player moving right
        else if (state_rightKeyDown) {
            player.xForce = player.mvSpeed;
            player.direction = 1;
        }

        // Check if falling / jumping
        if (state_upKeyDown && fuel > 0) {
            jump(player);
            fuel--;
        } else {
            player.yForce = -player.jmpSpeed;
        }

        // Check if the player gets hit by an enemy
        for (var i = 0; i < enemies.length; i++) {
            if (intersect(enemies[i], player)) {
                win = false;
            }
        }

        handleLevelCollisionFor(player);

        if (countDownSeconds < constTimerStart && intersect(ship, player)) {
            win = true;
        } else if (player.x < -500 || player.x > 1100 || player.y > 1500) {
            // If they go beyond thses bounds there is no way for the player to get back to the level
            win = false;
        }
    }


    // Update all enemy entities
    // Preconditions: None
    function updateEnemies() {
        var enemy;
        var changeX;
        var changeY;

        for (var i = 0; i < enemies.length; i++) {
            enemy = enemies[i];

            // Make the enemies move
            changeX = Math.random();
            changeY = Math.random();

            // Change x axis direction
            if (changeX >= 0.95) {
                if (enemy.direction == 1) { // If were heading right move left.
                    enemy.direction = -1;
                    enemy.xForce = -enemy.mvSpeed;
                } else {
                    enemy.direction = 1;
                    enemy.xForce = enemy.mvSpeed;
                }
            }

            // Change y axis direction
            if (changeY >= 0.9) {
                if (enemy.yForce > 0) { // If were heading down move up
                    jump(enemy);
                } else {    // Move down
                    enemy.yForce = -enemy.jmpSpeed;
                }
            }

            if (enemy.x + enemy.xForce > 700) {
                enemy.xForce = -enemy.mvSpeed;
            } else if (enemy.x + enemy.xForce < -100) {
                enemy.xForce = enemy.mvSpeed;
            }

            if (enemy.y + enemy.yForce < -50) {
                enemy.yForce = -enemy.jmpSpeed;
            } else if (enemy.y + enemy.yForce > 600) {
                enemy.yForce = enemy.jmpSpeed;
            }

            // Handle collisions
            handleLevelCollisionFor(enemy);
        }
    }


    // Update all projectiles
    // Preconditions: None
    function updateProjectiles() {
        for (var i = 0; i < projectiles.length; i++) {
            handleProjectileCollisions(projectiles[i], i);
        }
    }


    // Update the player score by a provided value
    // Preconditions: None
    function updateScore(value) {
        score += value;
    }


    // Update the timer for the game
    // Preconditions: None
    function updateTimer() {
        countDownSeconds -= 1;
        if (countDownSeconds > 0) {
            generateEnemies(6);
            timerTimeOut = setTimeout('updateTimer()', 1000);
        } else {
            clearTimeout(timerTimeOut);
        }
    }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// FUNCTIONS - Collision Handling / Boundry Checks & Accessors ///////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //==================================================================//
    //////// Collision Handling //////////////////////////////////////////
    //==================================================================//

    // Handle collisions between the entity and the level
    // Preconditions: Will only calculate within the bounds of the level array.
    function handleLevelCollisionFor(entity) {
        // Check if entity is within the x bounds of the level
        if (withinXLevelBounds(entity) && withinYLevelBounds(entity)) {
            // Check if the entity is witin the y bounds of the level
            var topTile = getTopTile(entity.y);
            var rightTile = getRightTile(entity.x);
            var botTile = getBottomTile(entity.y);
            var leftTile = getLeftTile(entity.x);

            entity.onGround = false;

            // We are moving downward
            if (entity.yForce > 0 && botTile < constMapHeight - 1) {
                if ((entity.y + entity.height + entity.yForce) > (botTile * constTileHeight)
                    && getTileType(getTile(botTile, leftTile + 1)) == constIMPASSABLE) {
                    entity.onGround = true;
                    // Align the entity with the block
                    entity.yForce = (botTile * constTileHeight - entity.height) - entity.y;
                }
                                // // Diagonal right
                // else if (entity.xForce > 0 && rightTile < constMapWidth - 1) {
                	// if ((entity.x + entity.width + entity.xForce) > (rightTile * constTileWidth)
                    	// && getTileType(getTile(botTile, rightTile)) == constIMPASSABLE) {
                    	// entity.xForce = 0;
                    // }
                // }
                // // Diagonal left
                // else if (entity.xForce < 0 && leftTile > 0) {
                	// if ((entity.x + entity.xForce) < ((leftTile * constTileWidth) + constTileWidth)
                    	// && getTileType(getTile(botTile, leftTile)) == constIMPASSABLE) {
                    	// entity.xForce = 0;
                	// }
                // }
            }
            // We are moving upward
            if (entity.yForce < 0 && topTile > 0) {
                if ((entity.y + entity.yForce) < ((topTile * constTileHeight) + constTileHeight)
                    && getTileType(getTile(topTile, leftTile + 1)) == constIMPASSABLE) {
                    // Align the entity with the block
                    entity.yForce = entity.y - (topTile * constTileHeight + constTileHeight);
                }
            }

            // We are moving right
            if (entity.xForce > 0 && rightTile < constMapWidth - 1) {
                if ((entity.x + entity.width + entity.xForce) > (rightTile * constTileWidth)
                    && getTileType(getTile(botTile - 1, rightTile)) == constIMPASSABLE) {
                    entity.xForce = 0;
                }
                // // Diagonal Up
                // else if (entity.yForce < 0 && topTile > 0) {
                	// if ((entity.y + entity.yForce) < ((topTile * constTileHeight) + constTileHeight)
                    // && getTileType(getTile(topTile, rightTile)) == constIMPASSABLE) {
                // }
                // // Diagonal Down
                // else if (entity.yForce > 0 && botTile < constMapHeight - 1) {
                	// if ((entity.y + entity.yForce) < ((topTile * constTileHeight) + constTileHeight)
                    // && getTileType(getTile(topTile, rightTile)) == constIMPASSABLE) {
                // }
            }
            // We are moving left
            if (entity.xForce < 0 && leftTile > 0) {
                if ((entity.x + entity.xForce) < ((leftTile * constTileWidth) + constTileWidth)
                    && getTileType(getTile(botTile - 1, leftTile)) == constIMPASSABLE) {
                    entity.xForce = 0;
                }
            }
        }

        entity.x += entity.xForce;
        entity.y += entity.yForce;
    }


    // Handle collisions between projectiles and the world / enemies
    // Note: Pass in the projectile index to make removal easy
    // Preconditions: None
    function handleProjectileCollisions(p, pIndex) {
        // First check for level collisions with projectile p
        if (withinXLevelBounds(p) && withinYLevelBounds(p)) {
            var leftTile = getLeftTile(p.x);
            var rightTile = getRightTile(p.x);
            var currRow = getBottomTile(p.y) - 1;

            // Check to the right
            if (p.xForce > 0
                && (p.x + p.width + p.xForce) > (rightTile * constTileWidth)
                && getTileType(getTile(currRow, rightTile)) == constIMPASSABLE) {
                p.active = false;
            }

            // Check if going left
            else if ((p.x + p.xForce) < ((leftTile * constTileWidth) + constTileWidth)
                && getTileType(getTile(currRow, leftTile)) == constIMPASSABLE) {
                p.active = false;
            }
        }

        // check if the shot is < 175 points away from where it was shot
        if ((p.xForce > 0 && p.x - p.startX > 175)
            || (p.xForce < 0 && p.x - p.startX < -175)) {
            p.active = false;
        }

        // If the projectile has not collided with the level, check if it intersects any enemies
        if (p.active) {
            // Move the projetile forward
            p.x += p.xForce;

            for (var i = 0; i < enemies.length; i++) {
                if (intersect(enemies[i], p)) {
                    p.active = false;
                    updateScore(25);

                    // Remove the enemey that was hit
                    enemies.splice(i, 1);
                    // Foevery enemy removed, add 2.
                    generateEnemies(2);
                    break;
                }
            }
        }

        // Remove the projectile if it is no longer active
        if (!p.active) {
            projectiles.splice(pIndex, 1);
        }
    }


    //==================================================================//
    //////// Boundary Checks & Accessors /////////////////////////////////
    //==================================================================//

    // Checks if an entity is within x bounds
    // Preconditions: entity must have x coordinates and a width
    function withinXLevelBounds(entity) {
        return (entity.x > 0 && entity.x <= (constMapWidth * constTileWidth) - entity.width);
    }


    // Checks if an entity is within y bounds
    // Preconditions: entity must have y coordinates and a height
    function withinYLevelBounds(entity) {
        return (entity.y > 0 && entity.y <= (constMapHeight * constTileHeight));
    }


    // Returns the tile underneath this location in the levels array
    // Preconditions: Y must be withing the level bounds
    function getBottomTile(y) {
        return Math.floor(y / constTileHeight) + 1;
    }


    // Returns the tile above this location in the levels array
    // Preconditions: Y must be withing the level bounds
    function getTopTile(y) {
        return Math.floor(y / constTileHeight) - 1;
    }


    // Returns the tile to the right of this location in the levels array
    // Preconditions: X must be withing the level bounds
    function getRightTile(x) {
        return Math.ceil(x / constTileWidth);
    }


    // Returns the tile to the left of this location in the levels array
    // Preconditions: X must be withing the level bounds
    function getLeftTile(x) {
        return Math.round(x / constTileWidth) - 1;
    }


    // Checks  if two different entities intersect one another
    // Preconditions: Both entities must have width, hieght, x, and y values
    function intersect(entity1, entity2) {
        return ((entity2.x >= entity1.x && entity2.x <= entity1.x + entity1.width)
            || ((entity2.x + entity2.width) >= entity1.x && (entity2.x + entity2.width) <= entity1.x + entity1.width))

            && ((entity2.y >= entity1.y && entity2.y <= entity1.y + entity1.height)
            || ((entity2.y + entity2.height) >= entity1.y && (entity2.y + entity2.height) <= entity1.y + entity1.height));
    }

    // For when you only want to draw what is in our view
    // Preconditions: context & constResX & constResY must have values, slcak has to be a number
    function onScreen(x, y, slack) {
        return (x >= context.offsetX - slack && x <= context.offsetX + constResX)
            && (y >= context.offsetY - slack && y <= context.offsetY + constResY);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// FUNCTIONS - Menus /////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Displays the start screen
    // Preconditions: None
    function displayStartScreen() {
        context.drawImage(startScreenImg, context.offsetX, context.offsetY);
    }


    // Displays the win screen
    // Preconditions: None
    function displayWinScreen() {
        context.drawImage(winImg, context.offsetX, context.offsetY);
        context.fillStyle = "Black";
        context.font = "30pt Courier";
        context.fillText(score*2500, context.offsetX+80, context.offsetY+(constResY/2)+5);
    }


    // Displays the lose screen
    // Preconditions: None
    function displayLoseScreen() {
        context.drawImage(loseImg, context.offsetX, context.offsetY);
    }
